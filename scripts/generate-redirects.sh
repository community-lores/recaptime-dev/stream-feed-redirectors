#!/usr/bin/env bash
set -x
REPO_ROOT=$(git rev-parse --show-toplevel)
REDIRECTS_YML_STATIONS=$REPO_ROOT/db/stations.yml

# Truncate the template file and the resulting file so for this build.
truncate -s 0 $REPO_ROOT/static/station_redirects
truncate -s 0 $REPO_ROOT/src/_redirects

for slug in $(yq -r 'to_entries[] | "\(.key)"' $REDIRECTS_YML_STATIONS); do
  REDIRECT_TARGET=$(yq -r .[\""$slug"\"] "$REDIRECTS_YML_STATIONS")
  echo "/feeds/$slug $REDIRECT_TARGET" | tee --append $REPO_ROOT/static/station_redirects
done

cat $REPO_ROOT/static/station_redirects | tee --append $REPO_ROOT/src/_redirects
cat $REPO_ROOT/static/base_redirects | tee --append $REPO_ROOT/src/_redirects