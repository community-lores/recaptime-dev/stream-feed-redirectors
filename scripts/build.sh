#!/usr/bin/env bash
set -x

pip3 install -r requirements.txt
$(dirname $0)/genetate-redirects.sh
mkdocs build