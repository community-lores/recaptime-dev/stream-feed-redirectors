# Stream Feed Redirectors

Basically a repo with `_redirects` and some Mkdocs stuff for radio stream feeds.

## Usage

Point your online radio client or your browser to the following URL:

```plaintext
https://listen.lorebooks.wip.la/feeds/<identifier>
```

where `<identifier>` is one of the available identifiers via `stations.index.json` file at
<https://listen.lorebooks.wip.la/stations.index.json> or [in the source directory](./src/stations.index.json).

## License

Website content: [CC BY-SA 4.0](./LICENSE.CC-BY-SA)

Source code: [GPL](./LICENSE)